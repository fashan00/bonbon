using AspnetApp.Utils;

namespace AspnetApp.Services
{
    public abstract class BaseService
    {
        protected IUnitOfWork _unitOfWork;
        public BaseService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

    }
}