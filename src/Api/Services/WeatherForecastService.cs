
using System;
using System.Collections.Generic;
using System.Linq;
using AspnetApp.Models;
using AspnetApp.Utils;
using Dapper;

namespace AspnetApp.Services
{
    public interface IWeatherForecastService
    {
        ResponseDetail Create(WeatherForecast weatherForecast);
        ResponseDetail Update(WeatherForecast weatherForecast);
        ResponseDetail Delete(int id);
        ResponseDetail GetListPaged();
        ResponseDetail Get(int id);
    }

    public class WeatherForecastService : BaseService, IWeatherForecastService
    {
        public WeatherForecastService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public ResponseDetail Create(WeatherForecast weatherForecast)
        {
            var rowId = _unitOfWork.Connection.Insert(weatherForecast, transaction: _unitOfWork.Transaction);
            return new ResponseDetail(rowId);
        }

        public ResponseDetail Update(WeatherForecast weatherForecast)
        {
            var rowId = _unitOfWork.Connection.Update(weatherForecast, transaction: _unitOfWork.Transaction);
            return new ResponseDetail(rowId);
        }

        public ResponseDetail Delete(int id)
        {
            var rowId = _unitOfWork.Connection.Delete<WeatherForecast>(id, transaction: _unitOfWork.Transaction);
            return new ResponseDetail(
                success: rowId == 1,
                message: rowId == 1 ? "delete sucess" : "delete failed"
            );
        }

        public ResponseDetail GetListPaged()
        {
            var rows = _unitOfWork.Connection.GetListPaged<WeatherForecast>(1, 3, "where 1=1", "temperature_c ASC").ToList();
            return new ResponseDetail(rows);
        }

        public ResponseDetail Get(int id)
        {
            var row = _unitOfWork.Connection.Get<WeatherForecast>(id);
            return new ResponseDetail(row);
        }

    }
}