using System;
using System.Data;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace AspnetApp.Utils
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool _disposed;
        public IDbTransaction Transaction { get; private set; }
        public IDbConnection Connection { get; private set; }

        public UnitOfWork(IConfiguration configuration)
        {
            Dapper.SimpleCRUD.SetDialect(Dapper.SimpleCRUD.Dialect.PostgreSQL);
            string connStr = configuration.GetConnectionString("Postgres");
            Connection = new NpgsqlConnection(connStr);
            Connection.Open();
        }

        public void BeginTransaction()
        {
            Transaction = Connection.BeginTransaction();
        }

        public void Commit() => Transaction?.Commit();

        public void Rollback() => Transaction?.Rollback();

        ~UnitOfWork() => Dispose(false);

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                Transaction?.Dispose();
                Connection?.Dispose();
            }

            Transaction = null;
            Connection = null;
            _disposed = true;
        }
    }
}