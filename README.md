
[.Net Core Tutorial](https://blog.johnwu.cc/article/ironman-day01-asp-net-core-starting.html)
==================

## DI
[Register Service By Scrutor](https://andrewlock.net/using-scrutor-to-automatically-register-your-services-with-the-asp-net-core-di-container/)


Repository Pattern/Transaction
==================

Dapper + Unit of work(UoW) + SimpleCRUD


DB Migration - [EF CORE](https://www.entityframeworktutorial.net/efcore/entity-framework-core.aspx)
==================







Auth
==================

## IdentityServer4

[OAuth 2.0 Flow](https://openhome.cc/Gossip/Spring/OAuth2Flows.html)

- [Client Credentials](https://dotblogs.com.tw/Null/2020/08/11/160828) 
- [Password](https://dotblogs.com.tw/Null/2020/08/11/172831)

- [Policy](https://blog.csdn.net/sD7O95O/article/details/107572013)


Error Handling
==================

[Http Error Code](https://blog.restcase.com/rest-api-error-codes-101/)
==================

- 200 ResponseDetail.cs 
- 400 ResponseFilter.cs
- 500 [CustomErrorHelper.cs](https://andrewlock.net/creating-a-custom-error-handler-middleware-function/amp/)









Deploy
==================

https://spin.atomicobject.com/2020/03/04/dockerizing-asp-net-react/

To build the image, run:
```
$ docker build -t sample-app .
```
And finally, to run your new Dockerized app:
```
$ docker run -p 80:80 -e "ConnectionStrings:Postgres=Host=192.168.7.120;User ID=postgres;Password=postgres@1234;Port=5432;Database=bonbon;" sample-app 
```
Now, you should be able to visit http://localhost:80 and see your app!
